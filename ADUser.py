class ADUserObject(object):
    """Class for AD User Opjects"""


    def __init__(self,
                 FirstName=None,
                 LastName=None,
                 SAM=None,
                 DisplayName=None,
                 EmailAddress=None,
                 EmailAppend=None,
                 EmailAliases=None):
        self.FirstName = FirstName
        self.LastName = LastName
        self.SAM = SAM
        self.DisplayName = DisplayName
        self.EmailAddress = EmailAddress
        self.EmailAppend = EmailAppend
        self.LenghtError= None
        self.EmailAliases = EmailAliases or []
        

    def About(self):
        print("""Author         : Harold Clark  email address harold.clark@openscg.com
Class          : ADUserObject
Inputs         : FirstName, LastName, SAM, DisplayName, EmailAddress
Returns        : none
Output         : csv file
Purpose        : This file Create a Active Directory User Object for define AD fields.  

""")
 

    def CreateDisplayName(self):
        """Creating a Display Name from first and Last names"""
        self.DisplayName = """%s %s""" % (self.FirstName, self.LastName) 


    def CreateDictionary(self):
        """Create a Dictionary for csv Export Functions"""
        D = {'GivenName':self.FirstName, 'Surname':self.LastName}
        D['DisplayName'] = self.DisplayName
        D['EMailAddress'] = self.EmailAddress
        D['SamAccountName'] = self.SAM
        return D
    
    def CreateProxyDictionary(self):
        P = []
        if not len(self.EmailAliases)==0:
            for a in self.EmailAliases:
                P.append({'SAM':self.SAM, 'EmailAddress':self.EmailAddress, 'DisplayName': self.DisplayName, 'ProxyAddress':a})
        return P
    
                
    def GetDictionaryKeys(self):
        """Return a list of the keys names for the Dictionary"""
        return ['GivenName','Surname','DisplayName','EMailAddress','SamAccountName']
    
    
    def GetProxyDictionaryKeys(self):
        """Return a list of the keys names for the ProxyAddress Dictionary"""
        return ['SAM','DisplayName','EmailAddress', 'ProxyAddress']

    def CreateEmail(self, EType="FL"):
        """Create an email address from frist and Last Name"""
        if EType=="FL":
            self.EmailAddress = """%s.%s@%s""" % (self.FirstName.lower(),
                                                  self.LastName.lower(),
                                                  self.EmailAppend)


    def CreateEmailAlias(self, AType='First'):
        """Create email Alias and appends it to EmailAliases list"""        
        NewAlias = None    
        if AType=='FirstLast':
            NewAlias = """%s%s@%s""" % (self.FirstName, self.LastName, self.EmailAppend)
        elif AType=='FIL':
            NewAlias = """%s%s@%s""" % (self.FirstName[0], self.LastName, self.EmailAppend)
        elif AType=='Initials':
            NewAlias = """%s%s@%s""" % (self.FirstName[0], self.LastName[0], self.EmailAppend)
        else:    
            NewAlias = """%s@%s""" % (self.FirstName, self.EmailAppend)
        self.EmailAliases.append(NewAlias)
        
        
    def CreateSAM(self, SAMType="FL"):
        """Create a SAM from Firstname.lastname='FL', FistInialLastName='FIL', or EmialID='EID'"""
        SAMType = SAMType.upper()
        if SAMType=="FL":
            self.SAM = """%s.%s""" % (self.FirstName, self.LastName)
        elif SAMType=="FIL":
            self.SAM = """%s%s""" % (self.FirstName[0].lower(), self.LastName.lower())
        elif SAMType=="EID":
            self.SAM = self.EmailAddress[0:self.EmailAddress.find("@")]
        if len(self.SAM) >20:
            self.LenghtError = True
        else:
            self.LenghtError = False
        return self.SAM


    def PrintUserInfo(self):
        """ Display User Info"""
        S = """FristName: %s
LastName: %s
SAM: %s
Display Name: %s
Email Address: %s
Email Aliases: %s""" % (self.FirstName,
                        self.LastName,
                        self.SAM,
                        self.DisplayName,
                        self.EmailAddress,
                        self.EmailAliases)
        print S

    def CheckLengthError(self): 
        if self.LenghtError==None:
            return "SAM Not set"
        elif self.LenghtError==True:
            return """%s is to long, abd need to shorten. Name will not be write out.""" % (self.SAM)
        elif self.LenghtError==False:
            return "SAM Lenght in Spec, file"
        

if __name__ == "__main__":
    o = ADUserObject(FirstName='Harold',
                     LastName="Clark",
                     SAM="Hald Clark",
                     DisplayName="rold Clark",
                     EmailAddress="Harold.Clark@openSCG.com",
                     EmailAppend="acompany.com")
    o.CreateDisplayName()
    o.CreateSAM(SAMType="FL")
    o.CreateEmail(EType="FL")
    o.PrintUserInfo()
    
    
    print """ """
    print """ """
    o.About()
