param([string]$Import="Import.csv")
Write-Output "Starting import ....."
write-Output "using file $Import"
Write-Output "       "
$report = @()
Import-Module ActiveDirectory
$Users = Import-Csv -Delimiter "," -Path ".\$Import"
$ErrorCount = 0
$ErrorAD = 0
$Count = 0
$LenghtBad = 0
$Exists = 0
$OU = "CN=Users,DC=acompany,DC=com"
$Password = (ConvertTo-SecureString "Password" -AsPlainText -Force)
foreach ($User IN $Users)
{
    $Count++
    $Detailedname = $User.DisplayName
    $Given = $User.GivenName
    $Sur = $User.SurName
    $SAM = $User.SamAccountName
    $Email = $User.EMailAddress
    $tempreport = New-Object PSObject
    $tempreport | Add-Member NoteProperty SAM $SAM
    Write-Output "User: $SAM"

    try {        
        New-ADUser -Name $Detailedname `
            -SamAccountName $SAM `
            -UserPrincipalName $SAM `
            -DisplayName $Detailedname `
            -GivenName $Given `
            -Surname $Sur `
            -AccountPassword $Password `
            -Enabled $true `
            -Path $OU `
            -EmailAddress $Email `
            -ErrorAction Stop
        $tempreport | Add-Member NoteProperty Result "Created"
        Write-Output "Created" 
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityAlreadyExistsException] 
    {
        $ErrorCount++
        $Exists++
        $tempreport | Add-Member NoteProperty Result "Exists"
        Write-Output "Created"
    }
    catch [Microsoft.ActiveDirectory.Management.ADAccount]
    {
        $ErrorCount++
        $ErrorAD++
        $NameLenght = $SAM.Length
        $tempreport | Add-Member NoteProperty Result "Length"
        Write-Output "ADAcount error: $_"
        Write-Output "Length: $NameLenght"
    }
    catch 
    {
        $ErrorCount++
        Write-Output "Error occured with $SAM"
        Write-Output "Else Error: $_"       
        $tempreport | Add-Member NoteProperty Result $_
    }
    Finally 
    {
    $report += $tempreport
    Write-Output " "
    Write-Output "******"
    }

}
$report | Export-Csv IMPORT_REPORT.csv -NoTypeInformation
Write-Output "Finish import"
Write-Output " "
Write-output "Here is what happen"
Write-Output "-----------------------------"
Write-Output "Attempted Accounts: $Count"
Write-Output "Error Count: $ErrorCount"
Write-Output "Error AD Count: $ErrorAD"
Write-Output "Lenght Count: $LenghtBad"
Write-Output "Exist Count: $Exists"
Write-Output "-----------------------------"
Write-Output "Have a good Day!"