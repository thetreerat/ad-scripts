#from ADUser import ADUserObject
from ADList import ActiveUsers
from ADList import DeletedUsers

if __name__ == "__main__":
    print "Starting import ..."
    Users = ActiveUsers()
    Users.ImportFile(f="OPENSCG_IMPORT.csv", EmailAppend='openscg.com')
    Users.PrintUserList('Short')
    DeletedUsers = DeletedUsers()
    DeletedUsers.ImportFile(f="OPENSCG_Delete.csv",EmailAppend='openscg.com')
    DeletedUsers.PrintUserList('short')
    
    
    DeletedUsers.RemoveFromActive(Users)
    
    
    #print "Starting Export ..."         
    Users.ExportFile()
    Users.PrintUserList('Short')
