import csv
from abc import ABCMeta, abstractmethod
from ADUser import ADUserObject


class ADList(object):
    """Base Class for a list of AD User Objects"""
    
    __metaclass__ = ABCMeta
    
    KeyType=None
    
    def __init__(self, Users=None):
        self.Users = Users or []
        
    def About(self):
        print("""Author         : Harold Clark  email address harold.clark@openscg.com
Class          : UserList
Inputs         : CSV File Name
Returns        : none
Output         : csv file
Purpose        : This Class creates a List of Active Directory Users.  

""")

    def ExportFile(self, f='import_output.csv'):
        """Export the List of users for use with power shell scripts"""
        with open(f, 'w') as output:
            writer = csv.DictWriter(output, fieldnames=self.Users[0].GetDictionaryKeys())
            writer.writeheader()
            for l in self.Users:
                if not l.LenghtError:
                    writer.writerow(l.CreateDictionary())
                else:
                    print l.CheckLengthError()


    def ImportFile(self, f='IMPORT.csv', EmailAppend='acompany.com'):
        """Import a CSV list into a list of user Objects"""
        with open(f) as ImportFile:
            csvList = csv.DictReader(ImportFile)
            for User in csvList:
                u = ADUserObject(EmailAppend=EmailAppend)
                u.FirstName = User['GivenName']
                u.LastName = User['Surname']
                if User['DisplayName']=='':
                    u.CreateDisplayName()
                else:
                    u.DisplayName = User['DisplayName']
                u.EmailAddress = User['EMailAddress']
                if User['SamAccountName']=='':
                    u.CreateSAM(SAMType="EID")
                else:
                    u.SAM = User['SamAccountName']
                #u.PrintUserInfo()
                #print """ """
                if self.ListType()=='Active Users' or self.ListType()=='Deleted Users':
                    self.Users.append(u)
                    #self.AddUser(u)
                else:
                    print """I am on vaction!, type: %s""" % (self.ListType())
                    
                
    def AddUser(self, user=None):
        """Check to see if Exist, then add to the list"""
        for u in self.Users:
            if user.SAM==u.SAM:
                print """user %s exist in list!, not added""" % (user.SAM)
                exit
            if user.EmailAddress==u.EmailAddress:
                print """EmailID: %s in use, not added""" % (user.EmailAddress)
                exit
        self.Users.append(user)


    def PrintUserList(self, DType='Long'):
        """ Display User List"""
        print """Displaying list of %s""" % (self.ListType())
        if DType=='Long':
            for s in self.Users:
                print """   %s""" % (s.DisplayName)
        print """Total Users: %s""" % (len(self.Users))

    @abstractmethod
    def ListType():
        """Return a string represting the list type"""
        pass
    

class ActiveUsers(ADList):
    """A list of Active Users"""
    KeyType='SAM'
    
                   
    def ListType(self):
        """Return a string represting the list type"""    
        return "Active Users"
    
    
    def ExportProxyList(self, f='ProxyAddress.CSV'):
        """Export User proxy address from User object list"""
        
        with open(f, 'w') as output:
            writer = csv.DictWriter(output, fieldnames=self.Users[0].GetProxyDictionaryKeys())
            writer.writeheader()
            for user in self.Users:
                P = user.CreateProxyDictionary()
                for p in P:
                    writer.writerow(p)
                    
                    
class DeletedUsers(ADList):
    """A List of Deleted Users for AD User List"""
    KeyType = "EmailAddress"
    
    def RemoveFromActive(self, AList):
        """Remove Deleted Users from the Acitve List on Class KeyID"""
        print len(self.Users)
        for d in self.Users:
            i = 0
            while i < len(AList.Users):
                #print """%s - avtive= %s - Deleted= %s""" % (i,
                #                                             AList.Users[i].DisplayName,
                #                                             d.DisplayName)
                if AList.Users[i].SAM==d.SAM:
                    print """%sis a deleted user removing""" % (d.DisplayName)
                    del AList.Users[i]
                    break
                
                i+=1
    

    def ListType(self):
        return 'Deleted Users'
    
if __name__ == "__main__":
    o = ADUserObject(FirstName='Harold',
                     LastName="Clark",
                     SAM="Hald Clark",
                     DisplayName="rold Clark",
                     EmailAddress="Harold.Clark@openSCG.com",
                     EmailAppend="acompany.com")
    o.CreateDisplayName()
    o.CreateSAM(SAMType="FL")
    o.CreateEmail(EType="FL")
    o.PrintUserInfo()
    
    
    print """ """
    print """ """
