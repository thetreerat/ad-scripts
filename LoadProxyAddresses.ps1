Write-Output "Starting import ....."
Write-Output " "

Import-Module ActiveDirectory
$Proxies = Import-Csv -Delimiter "," -Path ".\OPENSCG_ProxyAddress.CVS"
$Count = 0
foreach ($User in $Proxies)
{
    $Count++
    
    $ProxyAddress = $User.ProxyAddress
    $DisplayName = $User.DisplayName
    $SAM = $User.SAM
    Write-Output "$DisplayName adding $ProxyAddress"
    
    Set-ADUser -identity $SAM -add @{'ProxyAddresses'=@($ProxyAddress)} 
}
Write-Output "Done. Have a nice day!"